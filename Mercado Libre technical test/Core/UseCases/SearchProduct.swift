//
//  SearchProduct.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation

protocol SearchProduct {
    func execute(query: ProductQuery,
                 completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable?
}

final class DefaultSearchProduct: SearchProduct {
    private let productRepository: ProductRepository

    init(productRepository: ProductRepository) {
        self.productRepository = productRepository
    }

    func execute(query: ProductQuery,
                 completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable?
    {
        return productRepository.search(query: query) { result in
            completion(result)
        }
    }
}
