//
//  ProductRepository.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation
// sourcery: AutoMockable
protocol ProductRepository {
    @discardableResult
    func search(query: ProductQuery, completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable?
}
