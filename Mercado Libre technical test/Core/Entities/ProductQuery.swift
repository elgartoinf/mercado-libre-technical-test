//
//  ProductQuery.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation

struct ProductQuery {
    let query: String
}

extension ProductQuery: Equatable {}
