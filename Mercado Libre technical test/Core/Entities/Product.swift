//
//  Product.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation

struct Product {
    let id: String
    let title: String
    let thumbnail: String
}

extension Product: Equatable {}
extension Product: Codable {}
