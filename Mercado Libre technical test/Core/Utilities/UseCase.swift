//
//  UseCase.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation

public protocol UseCase {
    @discardableResult
    func start() -> Cancellable?
}
