//
//  HttpRequestService.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Alamofire
import Foundation
import SwiftyJSON

protocol APIService {
    static func request(endpoint: String, parameters: [String: Any]?, method: HTTPMethod, completion: @escaping (_ result: JSON, _ error: Error?) -> Void)
}

class HttpRequestService: APIService {
    static func request(endpoint: String, parameters: [String: Any]?, method: HTTPMethod, completion: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        Alamofire.request(endpoint, method: method, parameters: parameters ?? [:]).validate().responseJSON { response in
            switch response.result {
            case let .success(value):
                completion(JSON(value), nil)
            case let .failure(error):
                completion(JSON(), error)
            }
        }
    }
}
