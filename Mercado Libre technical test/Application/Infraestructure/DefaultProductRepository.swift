//
//  DefaultProductRepository.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 29/04/22.
//

import Foundation

final class DefaultProductRepository {
    private let httpService: APIService

    init(httpService: APIService) {
        self.httpService = httpService
    }
}

extension DefaultProductRepository: ProductRepository {
    func search(query: ProductQuery, completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable? {
        let searchProductsUrl = "https://api.mercadolibre.com/sites/MCO/search/"

        HttpRequestService.request(endpoint: searchProductsUrl, parameters: ["q": query.query], method: .get) { data, error in

            if error != nil {
                completion(.failure(error!))
                return
            }

            var products: [Product] = []
            for productDictionary in data["results"].arrayValue {
                let decodedProduct = try? JSONDecoder().decode(Product.self, from: productDictionary.rawData())
                if decodedProduct == nil {
                    continue
                }
                products.append(decodedProduct!)
            }
            completion(.success(products))
        }
        return nil
    }
}
