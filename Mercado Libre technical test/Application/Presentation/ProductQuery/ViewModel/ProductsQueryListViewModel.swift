//
//  ProductsQueryListViewModel.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation

import Foundation

typealias ProductsQueryListViewModelDidSelectAction = (ProductQuery) -> Void

protocol ProductsQueryListViewModelInput {
    func didSelect(item: ProductsQueryListItemViewModel)
}

protocol ProductsQueryListViewModelOutput {
    var items: Observable<[ProductsQueryListItemViewModel]> { get }
}

protocol ProductsQueryListViewModel: ProductsQueryListViewModelInput, ProductsQueryListViewModelOutput {}

final class DefaultProductsQueryListViewModel: ProductsQueryListViewModel {
    private let numberOfQueriesToShow: Int
    private let didSelect: ProductsQueryListViewModelDidSelectAction?

    let items: Observable<[ProductsQueryListItemViewModel]> = Observable([])

    init(numberOfQueriesToShow: Int, didSelect: ProductsQueryListViewModelDidSelectAction? = nil) {
        self.numberOfQueriesToShow = numberOfQueriesToShow
        self.didSelect = didSelect
    }
}

extension DefaultProductsQueryListViewModel {
    func didSelect(item: ProductsQueryListItemViewModel) {
        didSelect?(ProductQuery(query: item.query))
    }
}
