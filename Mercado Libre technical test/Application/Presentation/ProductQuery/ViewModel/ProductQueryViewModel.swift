//
//  ProductQueryViewModel.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation
class ProductsQueryListItemViewModel {
    let query: String

    init(query: String) {
        self.query = query
    }
}

extension ProductsQueryListItemViewModel: Equatable {
    static func == (lhs: ProductsQueryListItemViewModel, rhs: ProductsQueryListItemViewModel) -> Bool {
        return lhs.query == rhs.query
    }
}
