//
//  ProductsListItemViewModel.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation

struct ProductsListItemViewModel: Equatable {
    let id: String
    let title: String
    let thumbnail: String
}

extension ProductsListItemViewModel {
    init(product: Product) {
        title = product.title
        id = product.id
        thumbnail = product.thumbnail
    }
}
