//
//  ProductsListViewModel.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation

struct ProductsListViewModelActions {
    let showProductDetail: (Product) -> Void
}

enum ProductListViewModelLoading {
    case fullScreen
    case nextPage
}

protocol ProductsListViewModelInput {
    func viewDidLoad()
    func didSearch(query: String)
    func didCancelSearch()
    func didSelectItem(at index: Int)
}

protocol ProductsListViewModelOutput {
    var items: Observable<[ProductsListItemViewModel]> { get }
    var loading: Observable<ProductListViewModelLoading?> { get }
    var query: Observable<String> { get }
    var error: Observable<String> { get }
    var isEmpty: Bool { get }
    var screenTitle: String { get }
    var emptyDataTitle: String { get }
    var errorTitle: String { get }
    var searchBarPlaceholder: String { get }
}

protocol ProductsListViewModel: ProductsListViewModelInput, ProductsListViewModelOutput {}

final class DefaultProductsListViewModel: ProductsListViewModel {
    var emptyDataTitle = ""
    var errorTitle = ""

    private let searchProduct: SearchProduct
    private let actions: ProductsListViewModelActions?

    private var productsLoadTask: Cancellable? { willSet { productsLoadTask?.cancel() } }
    private var products: [Product] = []

    let items: Observable<[ProductsListItemViewModel]> = Observable([])
    let loading: Observable<ProductListViewModelLoading?> = Observable(.none)
    let query: Observable<String> = Observable("")
    let error: Observable<String> = Observable("")
    var isEmpty: Bool { return items.value.isEmpty }

    let screenTitle = "Productos"
    let searchBarPlaceholder = "Buscar productos"

    init(searchProduct: SearchProduct, actions: ProductsListViewModelActions? = nil) {
        self.searchProduct = searchProduct
        self.actions = actions
    }

    private func setProducts(_ products: [Product]) {
        self.products = products
        items.value = products.map(ProductsListItemViewModel.init)
    }

    private func resetProducts() {
        items.value.removeAll()
    }

    private func load(query: ProductQuery, loading: ProductListViewModelLoading) {
        self.loading.value = loading

        productsLoadTask = searchProduct.execute(
            query: query,
            completion: { result in
                switch result {
                case let .success(products):
                    self.setProducts(products)
                case let .failure(error):
                    self.handle(error: error)
                }
                self.loading.value = .none
            }
        )
    }

    private func handle(error: Error) {
        self.error.value = "Error: \(error.localizedDescription)"
    }

    private func update(ProductQuery: ProductQuery) {
        resetProducts()
        load(query: ProductQuery, loading: .fullScreen)
    }
}

extension DefaultProductsListViewModel {
    func viewDidLoad() {}

    func didSearch(query: String) {
        guard !query.isEmpty else { return }
        update(ProductQuery: .init(query: query))
    }

    func didCancelSearch() {
        productsLoadTask?.cancel()
    }

    func didSelectItem(at index: Int) {
        actions?.showProductDetail(products[index])
    }
}
