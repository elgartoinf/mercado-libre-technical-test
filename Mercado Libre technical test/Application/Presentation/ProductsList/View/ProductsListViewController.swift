//
//  ProductsListViewController.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import UIKit

final class ProductsListViewController: UIViewController, StoryboardInstantiable, Alertable {
    @IBOutlet private var searchContainer: UIView!
    @IBOutlet private var resultContainer: UIView!
    @IBOutlet var resultMessage: UILabel!

    private var viewModel: ProductsListViewModel!
    private var productsListTableViewController: ProductsListTableViewController?
    private var searchController = UISearchController(searchResultsController: nil)

    static func create(with viewModel: ProductsListViewModel) -> ProductsListViewController {
        let view = ProductsListViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }

    private func bind(to viewModel: ProductsListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.query.observe(on: self) { [weak self] in self?.updateSearchQuery($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController.isActive = false
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == String(describing: ProductsListTableViewController.self),
           let destinationVC = segue.destination as? ProductsListTableViewController
        {
            productsListTableViewController = destinationVC
            productsListTableViewController?.viewModel = viewModel
        }
    }

    private func setupViews() {
        title = viewModel.screenTitle
        resultMessage.text = viewModel.emptyDataTitle
        setupSearchController()
    }

    private func updateItems() {
        productsListTableViewController?.reload()
    }

    private func updateLoading(_ loading: ProductListViewModelLoading?) {
        resultContainer.isHidden = true
        resultMessage.isHidden = true
        LoadingView.hide()

        switch loading {
        case .fullScreen: LoadingView.show()
        case .nextPage: resultContainer.isHidden = false
        case .none:
            resultContainer.isHidden = viewModel.isEmpty
            resultMessage.isHidden = !viewModel.isEmpty
        }

        productsListTableViewController?.updateLoading(loading)
    }

    private func updateSearchQuery(_ query: String) {
        searchController.isActive = false
        searchController.searchBar.text = query
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: viewModel.errorTitle, message: error)
    }

    private func setupSearchController() {
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = viewModel.searchBarPlaceholder
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = true
        searchController.searchBar.barStyle = .default
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.frame = searchContainer.bounds
        searchController.searchBar.autoresizingMask = [.flexibleWidth]
        searchContainer.addSubview(searchController.searchBar)
        definesPresentationContext = true
    }
}

extension ProductsListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, !searchText.isEmpty else { return }
        searchController.isActive = false
        viewModel.didSearch(query: searchText)
    }

    func searchBarCancelButtonClicked(_: UISearchBar) {
        viewModel.didCancelSearch()
    }
}

extension ProductsListViewController: UISearchControllerDelegate {}
