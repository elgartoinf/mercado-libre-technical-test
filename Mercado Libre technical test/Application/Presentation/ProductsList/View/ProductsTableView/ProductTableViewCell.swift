//
//  ProductTableViewCell.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import UIKit

final class ProductTableViewCell: UITableViewCell {
    static let reuseIdentifier = String(describing: ProductTableViewCell.self)

    @IBOutlet private var title: UILabel!
    @IBOutlet private var id: UILabel!
    @IBOutlet private var thumbnail: UIImageView!

    private var viewModel: ProductsListItemViewModel!

    func fill(with viewModel: ProductsListItemViewModel) {
        self.viewModel = viewModel

        title.text = viewModel.title
        id.text = viewModel.id
        thumbnail.load(url: viewModel.thumbnail)
    }
}
