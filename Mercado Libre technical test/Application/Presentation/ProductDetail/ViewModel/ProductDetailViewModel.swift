//
//  ProductDetailViewModel.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 1/05/22.
//

import Foundation

protocol ProductDetailViewModel {
    var title: String { get }
    var id: String { get }
    var thumbnail: String { get }
}

final class DefaultProductDetailViewModel: ProductDetailViewModel {
    let title: String
    var id: String
    var thumbnail: String

    init(product: Product) {
        title = product.title
        id = product.id
        thumbnail = product.thumbnail
    }
}
