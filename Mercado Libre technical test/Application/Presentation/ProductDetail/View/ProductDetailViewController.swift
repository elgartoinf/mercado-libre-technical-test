//
//  ProductDetailViewController.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 1/05/22.
//

import UIKit

final class ProductDetailViewController: UIViewController, StoryboardInstantiable {
    @IBOutlet private var thumbnail: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var idLabel: UILabel!

    private var viewModel: ProductDetailViewModel!

    static func create(with viewModel: ProductDetailViewModel) -> ProductDetailViewController {
        let view = ProductDetailViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    private func setupViews() {
        title = viewModel.title
        thumbnail.load(url: viewModel.thumbnail)
        idLabel.text = viewModel.id
        titleLabel.text = viewModel.title
    }
}
