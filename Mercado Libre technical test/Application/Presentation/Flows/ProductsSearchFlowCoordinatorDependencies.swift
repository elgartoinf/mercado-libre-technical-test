//
//  ProductsSearchFlowCoordinatorDependencies.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import UIKit

protocol ProductsSearchFlowCoordinatorDependencies {
    func makeProductsListViewController(actions: ProductsListViewModelActions) -> ProductsListViewController
    func makeProductsDetailsViewController(product: Product) -> UIViewController
}

final class ProductsSearchFlowCoordinator {
    private weak var navigationController: UINavigationController?
    private let dependencies: ProductsSearchFlowCoordinatorDependencies

    private weak var productsListVC: ProductsListViewController?
    private weak var productsQueriesSuggestionsVC: UIViewController?

    init(navigationController: UINavigationController, dependencies: ProductsSearchFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }

    func start() {
        let actions = ProductsListViewModelActions(showProductDetail: showProductDetail)
        let vc = dependencies.makeProductsListViewController(actions: actions)

        navigationController?.pushViewController(vc, animated: false)
        productsListVC = vc
    }

    private func showProductDetail(product: Product) {
        let vc = dependencies.makeProductsDetailsViewController(product: product)
        navigationController?.pushViewController(vc, animated: true)
    }
}
