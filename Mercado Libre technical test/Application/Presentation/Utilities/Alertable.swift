//
//  Alertable.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import UIKit

public protocol Alertable {}
public extension Alertable where Self: UIViewController {
    func showAlert(title: String = "", message: String, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: completion)
    }
}
