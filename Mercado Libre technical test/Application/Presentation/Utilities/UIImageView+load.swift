//
//  Utilities.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//
import UIKit

public extension UIImageView {
    func load(url: String) {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()

        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: URL(string: url)!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
}
