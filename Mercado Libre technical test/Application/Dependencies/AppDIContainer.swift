//
//  AppDIContainer.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation

final class AppDIContainer {
    lazy var httpService: APIService = HttpRequestService()

    func makeProductsSceneDIContainer() -> ProductsSceneDIContainer {
        let dependencies = ProductsSceneDIContainer.Dependencies(apiService: httpService)
        return ProductsSceneDIContainer(dependencies: dependencies)
    }
}
