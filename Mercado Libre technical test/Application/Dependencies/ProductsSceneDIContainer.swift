//
//  ProductsSceneDIContainer.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import UIKit

final class ProductsSceneDIContainer {
    struct Dependencies {
        let apiService: APIService
    }

    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func makeSearchProduct() -> SearchProduct {
        return DefaultSearchProduct(productRepository: makeProductRepository())
    }

    func makeProductRepository() -> ProductRepository {
        return DefaultProductRepository(httpService: dependencies.apiService)
    }

    // MARK: Product List

    func makeProductsListViewController(actions: ProductsListViewModelActions) -> ProductsListViewController {
        return ProductsListViewController.create(with: makeProductsListViewModel(actions: actions))
    }

    func makeProductsListViewModel(actions: ProductsListViewModelActions) -> ProductsListViewModel {
        return DefaultProductsListViewModel(searchProduct: makeSearchProduct(),
                                            actions: actions)
    }

    // MARK: Product Detail

    func makeProductsDetailsViewController(product: Product) -> UIViewController {
        return ProductDetailViewController.create(with: makeProductDetailViewModel(product: product))
    }

    func makeProductDetailViewModel(product: Product) -> ProductDetailViewModel {
        return DefaultProductDetailViewModel(product: product)
    }

    func makeProductsSearchFlowCoordinator(navigationController: UINavigationController) -> ProductsSearchFlowCoordinator {
        return ProductsSearchFlowCoordinator(navigationController: navigationController,
                                             dependencies: self)
    }
}

extension ProductsSceneDIContainer: ProductsSearchFlowCoordinatorDependencies {}
