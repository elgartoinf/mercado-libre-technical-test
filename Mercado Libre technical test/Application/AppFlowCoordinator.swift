//
//  AppFlowCoordinator.swift
//  Mercado Libre technical test
//
//  Created by gabriel on 30/04/22.
//

import Foundation
import UIKit

final class AppFlowCoordinator {
    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer

    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer)
    {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }

    func start() {
        let productsSceneDIContainer = appDIContainer.makeProductsSceneDIContainer()
        let flow = productsSceneDIContainer.makeProductsSearchFlowCoordinator(navigationController: navigationController)
        flow.start()
    }
}
