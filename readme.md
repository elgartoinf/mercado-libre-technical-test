[![pipeline status](https://gitlab.com/elgartoinf/mercado-libre-technical-test/badges/master/pipeline.svg)](https://gitlab.com/elgartoinf/mercado-libre-technical-test/-/commits/master)


![Alt text](Docs/ddd_estructure.png "Clean Architecture")

## Layers
* **Domain Layer** = Entities + Use Cases + Repositories Interfaces
* **Data Repositories Layer** = Repositories Implementations + HttpRequestService (Network)
* **Presentation Layer (MVVM)** = ViewModels + Views


Application Structure 

![Alt text](Docs/esctructure.jpeg "Applicacion structure")