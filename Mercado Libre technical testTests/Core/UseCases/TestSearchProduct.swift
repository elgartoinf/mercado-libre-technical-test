//
//  TestSearchProduct.swift
//  Mercado Libre technical testTests
//
//  Created by gabriel on 30/04/22.
//

@testable import Mercado_Libre_technical_test
import SwiftyMocky
import XCTest

class TestSearchProduct: XCTestCase {
    let productQuery = ProductQuery(query: "test")
    let aProduct = Product(id: "anId", title: "aTitle", thumbnail: "aThumbnail")
    private let productRepository = ProductRepositoryMock()
    // object under test
    private lazy var useCase: DefaultSearchProduct = .init(productRepository: productRepository)
    private var products: [Product] = []
    struct EquatableError: Equatable, Error { let message: String }
    private let anError = EquatableError(message: "abc")
    private var result: Result<[Product], Error>?

    func testUseCaseSuccesfullAndReturnsProducts() throws {
        given_a_products()

        when_use_case_is_executed()

        then_it_return_products()
    }

    func testUseCaseSuccesfullAndReturnsEmptyProducts() throws {
        given_an_empty_products()

        when_use_case_is_executed()

        then_it_return_an_empty_array()
    }

    func testUseCaseFailsWhenIsExecuted() throws {
        given_a_repository_fail_result()

        when_use_case_is_executed()

        then_it_failt()
    }

    func given_a_repository_fail_result() {
        Perform(productRepository, .search(query: .any, completion: .any, perform: { _, completion in
            completion(.failure(self.anError))
        }))
    }

    func given_a_repository_success_result() {
        Perform(productRepository, .search(query: .any, completion: .any, perform: { _, completion in
            completion(.success(self.products))
        }))
    }

    func given_an_empty_products() {
        products = []
        given_a_repository_success_result()
    }

    func given_a_products() {
        products = [aProduct]
        given_a_repository_success_result()
    }

    func when_use_case_is_executed() {
        _ = useCase.execute(query: productQuery, completion: { result in
            self.result = result
        })
    }

    func then_it_return_an_empty_array() {
        switch result {
        case let .success(products):
            XCTAssertEqual(products, [])
        default:
            XCTFail()
        }
    }

    func then_it_failt() {
        switch result {
        case let .failure(error):
            XCTAssertEqual(error as! TestSearchProduct.EquatableError, anError)
        default:
            XCTFail()
        }
    }

    func then_it_return_products() {
        switch result {
        case let .success(products):
            XCTAssertEqual(products, self.products)
        default:
            XCTFail()
        }
    }
}
